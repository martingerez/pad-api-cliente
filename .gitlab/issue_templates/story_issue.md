# Título
Deberá contener una breve descripción de la funcionalidad a desarrollar.

# Descripción

## User Story
Contendrá en primer instancia, el desarrollo en formato User Story de la funcionalidad a desarrollar. Siguiendo siempre el formato:

"Como `rol`quiero `funcionalidad` para `necesidad que cubre la funcionalidad`".

## Criterio de Aceptación.
Seguida a la US se agregará el criterio de aceptación de la misma, el cuá incluirá los requerimientos mínimos para que la historia de usuario sea aceptada.
Este deberá estar indicado con el subtítulo `Criterio de Aceptación` o bien con la forma simplificada `C. A.`
