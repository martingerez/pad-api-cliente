class ProductosController < ApplicationController
  URL_BASE = 'http://localhost:5000/api/producto'

  def index
    response = HTTParty.get(URL_BASE)

    data = JSON.parse(response.body)

    @productos = data
  end

  def edit
    # Llamar a la API de C# con GetByID y setear el form

    # @producto = #Lo que devuelva la api
  end

  def update
    # Llamar a la API de C# con
  end

  def new
    @producto = Producto.new
  end

  def create
    @producto = Producto.new(producto_params)

    # Llamada a la API de C#
  end

  def delete
  end

private

  def producto_params
    params.require(:producto).permit(:nombre, :descripcion, :precio, :categoria)
  end
end